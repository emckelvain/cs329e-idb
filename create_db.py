# beginning of create_db.py
import json
from models import app, db, Book

def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

def create_books():
    book = load_json('books.json')

    for oneBook in book:
        title = oneBook['title']
        isbn = oneBook['isbn']
        try:
        	image_url = oneBook['image_url']
        except:
        	image_url = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAflBMVEX/xJz////bmWyhakr/1bigaEefZkS3kX2oeFydYj/TvrOaY0P5v5f/yKLal2nosIqudVKbXjjt5eGocVHgp4PIiWGoc1T/277gs5fn29XNs6XCoZCaXDb59fO5k3+wg2vdzcXZxbvFp5fPt6qxhW7q39rTk2n28e67f1rRmXacEEpjAAAC7ElEQVR4nO3YbXeaMBiAYYEFja6bkdXON9B1uu7//8FJ7Npz1hP0kHRPQu/7a9qe52oCgqNs6I2kB3j3EKYfwvRDmH4I0w/hS7v9uq7HMbf3Eh4aY3QZdWbqITwUWhWxp/sLd42J3+cjXJSl9PA31Vt43L5sYGUrqmVbFVM+wl/PV+Bsfp9f+vL1oe0uokYzD+HkckSrU56/Cj+duxvF02cP4dRY4DzPhyrUFviUD1b4aIVVPlzh0t5mTsMVLrb/XoRDE0712y0clnBjPyryAQsb9faQDks4QRhJCBEilA8hQoTyIUSIUD6ECBHKhxAhQvkQIkQoH0KECOVDiBChfAgRIpQPIUKE8iFEiFA+hAgRyocQIUL5ECJEKB9ChAjlQ4gQoXwIESKUDyFChPIhRIhQPoQIEcqHECFC+RAiRCgfQoQI5UOIEKF8CBEilA8hQoTyIUSIUD6ECBHKhxAhQvkQIkQoH0KECOVDiBChfAgRIpQPIUKE8iFEiFA+hAgRyocQIUL5ECJEKB9ChB9U+BCR8C6M8HT/t9/f277FlLfw9FQVr6lZdBWewvmsSKD+wurq346j/sJU8hWW2kSbDiAs9Wa/WsTayhK3Kw+hqXddPybduhWWG8fqLcLtIfxUIVu2YxrXJtwg1Ot3mCpgi23XFt4gVE34oYI2bQ+pcVyFtwi183cjaVO2UzqXrwtN8JEC15ynV'
        description = oneBook['description']
        publisher_name = oneBook['publishers'][0]['name']
        author_name = oneBook['authors'][0]['name']
        newBook = Book(title = title, id = isbn, image_url = image_url, description = description, publisher_name = publisher_name, author_name = author_name)
        
        # After I create the book, I can then add it to my session. 
        db.session.add(newBook)
        # commit the session to my DB.
        db.session.commit()



create_books()
# end of create_db.py
