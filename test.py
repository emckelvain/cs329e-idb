import os
import sys
import unittest
from models import db, Book

class DBTestCases(unittest.TestCase):
    def test_source_insert_1(self):
        s = Book(title = 'C++', id=20, description = '',  image_url ='', publisher_name = '', author_name = '')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '20').one()
        self.assertEqual(str(r.id), '20')

        db.session.query(Book).filter_by(id = '20').delete()
        db.session.commit()

#Create rows, delete rows

#if __name__ == '__main__':

#    unittest.main()
