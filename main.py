from flask import Flask, render_template
app = Flask(__name__)

book1 = {"title": "Harry Potter and the Sorcerer's Stone", 
		"publisher": "Pottermore", 
		"author": "J. K. Rowling",
		"description": "\"Turning the envelope over, his hand trembling, Harry saw a purple wax seal bearing a coat of arms; a lion, an eagle, a badger and a snake surrounding a large letter 'H'.\" Harry Potter has never even heard of Hogwarts when the letters start dropping on the doormat at number four, Privet Drive. Addressed in green ink on yellowish parchment with a purple seal, they are swiftly confiscated by his grisly aunt and uncle. Then, on Harry's eleventh birthday, a great beetle-eyed giant of a man called Rubeus Hagrid bursts in with some astonishing news: Harry Potter is a wizard, and he has a place at Hogwarts School of Witchcraft and Wizardry. An incredible adventure is about to begin!",
		"isbn": "9781781100486",
      "image_url": "https://prodimage.images-bn.com/pimages/9780590353403_p0_v1_s550x406.jpg",
		"path" : "harry"
		}
		
book2 = {"title": "Mistborn", 
		"publisher": "Palgrave Macmillan",
		"author": "Brandon Sanderson",
		"description": "From #1 New York Times bestselling author Brandon Sanderson, the Mistborn series is a heist story of political intrigue and magical, martial-arts action. For a thousand years the ash fell and no flowers bloomed. For a thousand years the Skaa slaved in misery and lived in fear. For a thousand years the Lord Ruler, the \"Sliver of Infinity,\" reigned with absolute power and ultimate terror, divinely invincible. Then, when hope was so long lost that not even its memory remained, a terribly scarred, heart-broken half-Skaa rediscovered it in the depths of the Lord Ruler's most hellish prison. Kelsier \"snapped\" and found in himself the powers of a Mistborn. A brilliant thief and natural leader, he turned his talents to the ultimate caper, with the Lord Ruler himself as the mark. Kelsier recruited the underworld's elite, the smartest and most trustworthy allomancers, each of whom shares one of his many powers, and all of whom relish a high-stakes challenge. Only then does he reveal his ultimate dream, not just the greatest heist in history, but the downfall of the divine despot. But even with the best criminal crew ever assembled, Kel's plan looks more like the ultimate long shot, until luck brings a ragged girl named Vin into his life. Like him, she's a half-Skaa orphan, but she's lived a much harsher life. Vin has learned to expect betrayal from everyone she meets, and gotten it. She will have to learn to trust, if Kel is to help her master powers of which she never dreamed. This saga dares to ask a simple question: What if the hero of prophecy fails? The Cosmere The Mistborn series Mistborn: The Final Empire The Well of Ascension The Hero of Ages Alloy of Law Shadows of Self Bands of Mourning The Stormlight Archive The Way of Kings Words of Radiance Edgedancer (Novella) Oathbringer (forthcoming) Collection Arcanum Unbounded Other Cosmere Titles Elantris Warbreaker Rithmatist The Alcatraz vs. the Evil Librarians series Alcatraz vs. the Evil Librarians The Scrivener's Bones The Knights of Crystallia The Shattered Lens The Dark Talent The Reckoners Steelheart Firefight Calamity At the Publisher's request, this title is being sold without Digital Rights Management Software (DRM) applied.",
		"isbn": "1429914564",
      "image_url": "https://upload.wikimedia.org/wikipedia/en/4/44/Mistborn-cover.jpg",
		"path" : "mistborn"
		}
		
book3 = {"title": "All the President's Men",
		"publisher": "Simon & Schuster",
		"author": "Bob Woodward",
		"description": "It began with a break-in at the Democratic National Committee headquarters in Washington DC, on 17 June 1972. Bob Woodward, a journalist for the Washington Post, was called into the office on a Saturday morning to cover the story. Carl Bernstein, a political reporter on the Post, was also assigned. They soon learned this was no ordinary burglary. Following lead after lead, Woodward and Bernstein picked up a trail of money, conspiracy and high-level pressure that ultimately led to the doors of the Oval Office. Men very close to the President were implicated, and then Richard Nixon himself. Over a period of months, Woodward met secretly with Deep Throat, for decades the most famous anonymous source in the history of journalism. As he and Bernstein pieced the jigsaw together, they produced a series of explosive stories that would not only win the Post a Pulitzer Prize, they would bring about the President's scandalous downfall. ALL THE PRESIDENT'S MEN documents this amazing story. Taut, gripping and fascinating, it is a classic of its kind -- the true story of the events that changed the American presidency.",
		"isbn": "9781471104664",
      "image_url": "https://upload.wikimedia.org/wikipedia/en/2/25/All_the_President%27s_Men_book_1974.jpg",
		"path" : "presidents"}


author1 =  {"born": "1965-07-31",
            "name": "J. K. Rowling",
            "education": "Bachelor of Arts",
            "nationality": "British",
            "description": "Joanne \"Jo\" Rowling, OBE, FRSL, pen names J. K. Rowling and Robert Galbraith, is a British novelist, screenwriter and film producer best known as the author of the Harry Potter fantasy series. ",
            "alma_mater": "University of Exeter",
            "wikipedia_url": "https://en.wikipedia.org/wiki/J._K._Rowling",
            "image_url": "http://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/J._K._Rowling_2010.jpg/220px-J._K._Rowling_2010.jpg",
            "books": book1,
            "path": "jkr"
          }
            
author2 = {"born": "1975-12-19",
            "name": "Brandon Sanderson",
            "nationality": "American",
            "description": "Brandon Sanderson is an American fantasy and science fiction writer. He is best known for his Mistborn series and his work in finishing Robert Jordan's epic fantasy series The Wheel of Time. ",
            "alma_mater": "Brigham Young University (B.A., M.A.)",
            "wikipedia_url": "https://en.wikipedia.org/wiki/Brandon_Sanderson",
            "image_url": "http://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Brandon_Sanderson_sign.jpg/250px-Brandon_Sanderson_sign.jpg",
            "books": book2,
            "path": "bs"
            }
            
author3 = {"born": "1943-03-26",
            "name": "Bob Woodward",
            "nationality": "American",
            "education": "Yale University, BA, 1965",
            "description": "Robert Upshur \"Bob\" Woodward is an American investigative journalist and non-fiction author. He has worked for The Washington Post since 1971 as a reporter and is now an associate editor of the Post.\n",
            "wikipedia_url": "https://en.wikipedia.org/wiki/Bob_Woodward",
            "image_url": "http://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/Bob_Woodward.jpg/220px-Bob_Woodward.jpg",
            "books": book3,
         	"path": "bw"
            }


publisher1 = {"wikipedia_url": "https://en.wikipedia.org/wiki/Pottermore",
                "name": "Pottermore",
                "description": "Pottermore is the digital publishing, e-commerce, entertainment, and news company from J.K. Rowling and is the global digital publisher of Harry Potter and J.K. Rowling's Wizarding World.",
                "owner": "J. K. Rowling",
                "image_url": "http://upload.wikimedia.org/wikipedia/en/thumb/6/6f/Pottermore.png/225px-Pottermore.png",
                "website": "http://www.pottermore.com\nshop.pottermore.com",
                "books": book1,
                "authors": author1,
                "path" : "pottermore"
                }
                
publisher2 = {"founded": "2000",
                "name": "Palgrave Macmillan",
                "location": "London",
                "wikipedia_url": "https://en.wikipedia.org/wiki/Palgrave_Macmillan",
                "description": "Palgrave Macmillan is an international academic and trade publishing company. Its programme includes textbooks, journals, monographs, professional and reference works in print and online.\nPalgrave Macmillan was created in 2000 when St. ",
                "parent company": "Springer Nature",
                "image_url": "",
                "website": "http://www.palgrave.com",
                "books": book2,
                "authors": author2,
                "path": "palgrave"}
                
publisher3 = {"founded": "1924",
                "name": "Simon & Schuster",
                "location": "1230 Avenue of the Americas\nRockefeller Center,\nNew York City, New York",
                "wikipedia_url": "https://en.wikipedia.org/wiki/Simon_%26_Schuster",
                "description": "Simon &amp; Schuster, Inc., a division of CBS Corporation, is a publisher founded in New York City in 1924 by Richard L. Simon and M. Lincoln Schuster. It is one of the largest English-language publishers, formerly known as the \"Big 6\", now known as the \"Big Five\". It publishes over 2,000 titles annually under 35 different imprints.",
                "parent company": "CBS Corporation",
                "image_url": "http://upload.wikimedia.org/wikipedia/en/thumb/d/db/Simon_and_Schuster.svg/200px-Simon_and_Schuster.svg.png",
                "website": "http://www.simonandschuster.com",
                "books": book3,
                "authors": author3,
                "path" :"s&s"}

author1["publishers"] = publisher1
author2["publishers"] = publisher2
author3["publishers"] = publisher3
book1["authors"] = author1
book1["publishers"] = publisher1
book2["authors"] = author2
book2["publishers"] = publisher2
book3["authors"] = author3
book3["publishers"] = publisher3

bookDict = {book1["path"]: book1, book2["path"]: book2, book3["path"]: book3}
authorDict = {author1["path"]: author1, author2["path"]: author2, author3["path"]: author3}
publisherDict = {publisher1["path"]: publisher1, publisher2["path"]: publisher2, publisher3["path"]: publisher3}

@app.route('/')
def splash(): 
	return render_template('splash.html')

@app.route('/book/')
def book():
	return render_template('book.html')

@app.route('/author/')
def author():
	return render_template('author.html')

@app.route('/publisher/')
def publisher():
	return render_template('publisher.html')

@app.route('/book/<bookname>')
def book_profile(bookname):
	#return bookname
	return render_template('bookobj.html', dict = bookDict[bookname])

@app.route('/author/<authorname>')
def author_profile(authorname):
	return render_template('authorobj.html', dict = authorDict[authorname])
	#need to return author object template
	
@app.route('/publisher/<publishername>')
def publisher_profile(publishername):
	return render_template('publisherobj.html', dict = publisherDict[publishername])

@app.route('/about/')
def about():
	return render_template('about.html')
	
if __name__ == "__main__":
	app.run()
