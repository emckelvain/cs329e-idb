# beginning of models.py
# note that at this point you should have created "bookdb" database (see install_postgres.txt).
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://Biediger:biediger10@localhost/bookdb')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

class Book(db.Model):
	__tablename__ = 'book'
	title = db.Column(db.String(80), nullable = False)
	id = db.Column(db.Integer, primary_key = True)
	description = db.Column(db.String(1000), nullable = False)
	image_url = db.Column(db.String(1000), nullable = False)
	publisher_name = db.Column(db.String(80), nullable = False)
	author_name = db.Column(db.String(80), nullable = False)

class Author(db.Model):
	__tablename__ = 'author'
	born = db.Column(db.String(80), nullable = False)
	name = db.Column(db.String(80), primary_key = True)
	education = db.Column(db.String(80), nullable = False)
	nationality = db.Column(db.String(80), nullable = False)
	description = db.Column(db.String(1000), nullable = False)
	alma_mater = db.Column(db.String(80), nullable = False)
	wikipedia_url = db.Column(db.String(80), nullable = False)
	image_url = db.Column(db.String(80), nullable = False)

class Publisher(db.Model):
	__tablename__ = 'publisher'
	wikipedia_url = db.Column(db.String(80), nullable = False)
	name = db.Column(db.String(80), primary_key = True)
	description = db.Column(db.String(1000), nullable = False)
	owner = db.Column(db.String(80), nullable = False)
	image_url = db.Column(db.String(80), nullable = False)
	website = db.Column(db.String(80), nullable = False)
	
db.create_all()
# End of models.py
